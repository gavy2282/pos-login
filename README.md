=>
    pos-login (This API works to handle different requests realted to login)
    for salesmanlab

=>
    How to run it on local environment?
    
    1. open terminal / command line in root directory.
    2. run command "npm install" to fetch all dependencies.
    3. step 1 & 2 should be followed everytime you pull changes or clone repos from git.
    4. run command "npm start" to run server
    5. server will start running on localhost:3000/login/

=>
    What does it do?
    
    1. Create / Remove log in credetials in databse.
    2. To make an account active / non-active.
    3. Search log in related information of different users.
    4. Hashing password.
    5. Creating jwt(json web token) while logging in if email and password matches.

=>
    Database Schema ->

    CREATE DATABASE pos_login;
    CREATE TABLE login(
        companyId VARCHAR(100) NOT NULL,
        staffId VARCHAR(100) NOT NULL,
        userType VARCHAR(100) NOT NULL,
        active TINYINT(1) DEFAULT 0 NOT NULL,
        email VARCHAR(100) NOT NULL PRIMARY KEY UNIQUE,
        password VARCHAR(1000) NOT NULL);

=>
    How to use different endpoints of API. (https://localhost:3000/login)

    1. '/signIn' (POST) - to sign in 
        Send a json body which contains a valid email and password. for ex-
        {
            "email": "abc@xyz.com",
            "password": "password"
        }

    2.  '/search/email/:email' (GET) - to search user's login information using email
        Pass email as parameter in url ex- https://pos.com/login/search/email/"abc@xyz.com"

    3. '/search/companyId/:companyId' (GET) - to search company's users login information using companyId
        Pass companyId as parameter in url ex- https://pos.com/login/search/companyId/"123"

    4. '/createLogin' (POST) - to store login credentials to db
        Send a json body which contains companyId, email and password. for ex-
        {
            "companyId": "123",
            "email": "abc@xyz.com",
            "password": "password"
        }

    5.  '/update/email/:oldEmail' (PATCH) - to update email of a user
        Pass current email as parameter in url ex- https://pos.com/login/update/email/"current@email.com"
        and send new email as value in body for ex-
        {
            "newEmail": "new@email.com"
        }

    6.  '/update/activeEmail/:email' (PATCH) - to update email of a user
        Pass user's email as parameter in url ex- https://pos.com/login/update/activeEmail/"user@email.com"
        and send new active value in body for ex-
        {
            "newActive": 1
        }

    7.  '/update/activeCompany/:companyId' (PATCH) - to activate/ deactivate all users of a company
        Pass company's ID as parameter in url ex- https://pos.com/login/update/activeCompany/"123"
        and send new active value in body for ex-
        {
            "newActive": 0
        }

    8.  '/update/passwordEmail/:email' (PATCH) - to update password of a user
        Pass user's email as parameter in url ex- https://pos.com/login/update/passwordEmail/"user@email.com"
        and send new active value in body for ex-
        {
            "newPassword": "newPasswordIsThis"
        }

    9.  '/delete/email/:email' (DELETE) - to delete a particular user by email
        Pass user's email as parameter in url ex- https://pos.com/login/delete/email/"user@email.com"

    10. '/delete/company/:companyId' (DELETE) - to delete all users of a company by companyId
        Pass company's ID as parameter in url ex- https://pos.com/login/company/"123"